//
//  CommentsModel.swift
//  JsonDemo
//
//  Created by Kvana Inc 1 on 13/07/16.
//  Copyright © 2016 Kvana. All rights reserved.
//

import UIKit

class CommentsModel: NSObject {
    var postId: Int!
    var id:Int!
    var name: String!
    var email: String!
    var body: String!
    
    init(fromDictionary    dictinary: NSDictionary){
        postId = dictinary["postId"] as! Int
        id = dictinary["id"]as! Int
        name = dictinary["name"] as! String
        email = dictinary["email"]as! String
        body = dictinary["body"]as! String
        
    }
    

}
