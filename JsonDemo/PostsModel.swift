//
//  PostsModel.swift
//  JsonDemo
//
//  Created by Kvana Inc 1 on 13/07/16.
//  Copyright © 2016 Kvana. All rights reserved.
//

import UIKit

class PostsModel: NSObject {
    var body : String!
    var id : Int!
    var title : String!
    var userId : Int!
    
    init(fromDictionary dictionary: NSDictionary){
        body = dictionary["body"] as? String
        id = dictionary["id"] as? Int
        title = dictionary["title"] as? String
        userId = dictionary["userId"] as? Int
    }
}

