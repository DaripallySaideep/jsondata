//
//  ImageViewController.swift
//  JsonDemo
//
//  Created by Kvana Inc 1 on 13/07/16.
//  Copyright © 2016 Kvana. All rights reserved.
//

import UIKit
import SDWebImage

class ImageViewController: UIViewController {
    var passedValue = NSURL ()
    
@IBOutlet weak var detailImage: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        detailImage.sd_setImageWithURL(passedValue)
      }
 override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
           }
    
}
