//
//  ViewController.swift
//  JsonDemo
//
//  Created by Kvana Inc 1 on 01/07/16.
//  Copyright © 2016 Kvana. All rights reserved.
//

import UIKit
import Alamofire
import Kingfisher
import SDWebImage



class ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    var postArray=[PostsModel] ()
    var commentArray = [CommentsModel] ()
    var photoArray = [PhotosModel] ()
@IBOutlet weak var jsonTable: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        //posts()
       // comments()
        photos()
    }
    
    func posts() {
        Alamofire.request(.GET, "https://jsonplaceholder.typicode.com/posts")
            .responseJSON { response in
            if let JSON = response.result.value {
                for data in JSON as! [AnyObject]{
                    let details = PostsModel(fromDictionary: data as! NSDictionary)
                          self.postArray.append(details)
                        self.jsonTable .reloadData()
                }
                }
        }
    }
    func comments()  {
        
        Alamofire.request(.GET, "https://jsonplaceholder.typicode.com/comments")
            .responseJSON { response in
                if let JSON = response.result.value {
                    for data in JSON as! [AnyObject]{
                        let details = CommentsModel(fromDictionary: data as! NSDictionary)
                        self.commentArray.append(details)
                        self.jsonTable .reloadData()
                    }
                }
        }
        }
    func  photos(){
        Alamofire.request(.GET, "https://jsonplaceholder.typicode.com/photos")
            .responseJSON { response in
                if let JSON = response.result.value {
                    for data in JSON as! [AnyObject]{
                        let details = PhotosModel(fromDictionary: data as! NSDictionary)
                        self.photoArray.append(details)
                        self.jsonTable .reloadData()
                    }
                }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
           }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //return postArray.count
        //return commentArray.count
        return photoArray.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell:TitleTableViewCell = self.jsonTable.dequeueReusableCellWithIdentifier("TitleTableViewCell") as! TitleTableViewCell
     //  let posts = self.postArray[indexPath.row]
      //  let comments = self.commentArray[indexPath.row]
        let title = self.photoArray[indexPath.row]
        cell.userIdLbl.text = String (title.albumId)
        cell.titleLbl.text = title.title
        let url : NSString = title.thumbnailUrl
        let urlStr : NSString = url.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!
        let searchURL : NSURL = NSURL(string: urlStr as String)!
        cell.albumImage.sd_setImageWithURL(searchURL)
        return cell
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let title = self.photoArray[indexPath.row]
        let url : NSString = title.url
        let urlStr : NSString = url.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!
        let searchURL : NSURL = NSURL(string: urlStr as String)!
        let imgViewCon = self.storyboard!.instantiateViewControllerWithIdentifier("ImageViewController") as! ImageViewController
        imgViewCon.passedValue = searchURL
        self.navigationController!.pushViewController(imgViewCon, animated: true)
     
    }
}

