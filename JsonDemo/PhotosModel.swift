//
//  PhotosModel.swift
//  JsonDemo
//
//  Created by Kvana Inc 1 on 13/07/16.
//  Copyright © 2016 Kvana. All rights reserved.
//

import UIKit

class PhotosModel: NSObject {
    var albumId : Int!
     var id : Int!
     var title : String!
     var url : String!
    var thumbnailUrl : String!
    
    init(fromDictionary  dictionary:NSDictionary) {
        albumId = dictionary["albumId"] as! Int
         id = dictionary["id"] as! Int
         title = dictionary["title"] as! String
         url = dictionary["url"] as! String
         thumbnailUrl = dictionary["thumbnailUrl"] as! String
    }
}
